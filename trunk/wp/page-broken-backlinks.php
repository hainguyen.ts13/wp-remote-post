<?php
/**
 * Template Name: Broken Backlinks
 * version 1.0
 * @author: enginethemes
 **/

define('LINK_GET_BROKEN', 'https://tools.seobeginner.com/api/sba/remote-request/broken-backlinks?url=');

get_header(); ?>

<style>
    #customers {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #customers td, #customers th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #customers tr:nth-child(even){background-color: #f2f2f2;}

    #customers tr:hover {background-color: #ddd;}

    #customers th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }

    .link {
        color: #4CAF50;
    }

    .pre-text {
        color: #039BE5;
    }

    .status {
        background-color: #D84315;
        color: #ffffff;
        border-radius: 4px;
        padding: 2px;
    }
</style>

<?php
// define variables and set to empty values
$result = "first";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $result = validate($_POST["q"]);
    if (empty($result)) { // validation passed
        $link = LINK_GET_BROKEN.parse_url($_POST["q"], PHP_URL_HOST);
        $data_json = wp_remote_retrieve_body(wp_remote_get($link));
        $data = json_decode($data_json);
        $links = $data->refpages;
    }
}

function validate($data)
{
    $message = '';
    if (empty($data)) {
        $message = htmlspecialchars('Xin vui lòng nhập url.');
    } else if (!filter_var($data, FILTER_VALIDATE_URL)) {
        $message = htmlspecialchars('Url không hợp lệ. Format đúng: http://www.google.com');
    }
    return $message;
}

?>

<div class="main-holder content-wrapper clearfix">
    <div class="row">
        <div class="span12">
            <form method="POST" action="" class="form-inline">
                <input name="q" type="text" placeholder="Từ khoá: www.google.com" value="<?php echo $_POST["q"]; ?>"/>
                <div style="height: 20px;"></div>
                <button type="submit"> Tìm kiếm</button>
            </form>
        </div>
    </div>

    <?php if ($result == "first"): ?>

    <?php elseif (empty($result)): ?>
        <?php if (is_array($links)): ?>
            <hgroup class="mb20">
                <h1>Kết quả tìm kiếm</h1>
                <h2 class="lead">
                    <strong class="text-danger"><?php echo count($links) ?></strong> kết quả cho site <strong
                            class="text-danger"><?php echo parse_url($_POST["q"], PHP_URL_HOST); ?></strong>
                </h2>
            </hgroup>

            <section class="col-xs-12 col-sm-6 col-md-12">
                <table id="customers">
                    <tr>
                        <th>Refering page</th>
                        <th>Anchor and link</th>
                    </tr>
                    <?php foreach ($links as $link): ?>
                        <tr>
                            <td>
                                <span class="pre-text"><?php echo $link->title; ?></span><br>
                                <span class="link"><?php echo $link->url_from; ?></span>
                            </td>
                            <td>
                                <span class="link"><?php echo $link->url_to; ?></span><br>
                                <span class="pre-text"><?php echo $link->text_pre . ' (' . $link->anchor . ') ' . $link->text_post; ?></span><br>
                                <span class="status"><?php echo $link->http_code; ?></span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </section>
        <?php else: ?>
            <?php echo __('Không có link nào bị broken.'); ?>
        <?php endif; ?>
    <?php else: ?>
        <?php echo $result; ?>
    <?php endif; ?>
</div>

<?php get_footer(); ?>
