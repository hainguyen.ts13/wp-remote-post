## SEOBeginner Auto Post

### Description
SEOBeginer Auto Post allow user can insert a post remote.

- Contributors: SEOBeginner
- Tags: seo, insert post remote, webmaster tools, keywords, meta, meta description, meta title.
- Tested up to: 4.9.5
- Stable tag: 2.1
- Requires PHP: 5.3
- License: GPLv2 or later
- License URI: http://www.gnu.org/licenses/gpl-2.0.html

#### Features:
- Allow user can post a post from remote.
- User can get all categories of a site then select categoires that you want to set, Set post title, post thumbnail from an url then submit. Plugin auto check the password(code) and insert the post match with these information.
- When use send an request to http://yourdomain.com/?act=getcats, you can get all current categories of that website with ID and slug, base on that you can assign it into the request submit.

## Changelog:
### 1.1
* Allow user post with multi category.

### 1.2
- Reference Urls:
    + URL get old/current auto post: https://tools.seobeginner.com/api/guestpost/post-id-by-domain?domain=https://cozihay.com
    + URL return update result when active plugin [updated to new version]: https://cozihay.com/wp-admin/admin-ajax.php?action=get_storage_json
    
### 2.0
- Allow user to edit or delete post from plugin

### 2.1
- Fix bug