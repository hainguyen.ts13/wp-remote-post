jQuery(document).ready(function( $ ) {

    var client_ip = localStorage.getItem("client_ip") || null;
    var client_ip_experiod = localStorage.getItem("client_ip_experiod") || null;

    if (!client_ip_experiod || client_ip_experiod < new Date()) {
        var client_info = $.ajax({
            url: 'https://ipinfo.io/json',
            dataType: 'JSON',
            async:false
        });
        client_ip = client_info.responseJSON.ip
        
        localStorage.setItem("client_ip", client_ip);
        localStorage.getItem("client_ip_experiod", new Date() + 24*60*60*1000);
    }

    var fbid = getUrlParameter('fbid'),
        time_start = new Date()

    $(window).bind('beforeunload', function(){
        let duration = (new Date() - time_start)/1000
        
        //send session duration to server
        var data = {
            'action': 'sbap_sessions_duration',
            'duration': duration,
            'fbid': fbid,
            'full_url': window.location.href,
            'client_ip': client_ip,
            'session_token': sbap_object.session_token
        };

        $.post(sbap_object.ajax_url, data, function(response) {
           console.log(response.data);
        });
    })
});
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};